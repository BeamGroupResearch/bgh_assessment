from typing import Dict, List, Union

import pandas as pd
from sklearn.base import RegressorMixin, ClassifierMixin


def create_data_splits(
    df: pd.DataFrame, test_size_absolute: int
) -> Dict[str, pd.DataFrame]:
    """Slices input data into training and test splits. Conventional terminology adopted."""
    ...

    return dict(df_train=df, df_test=df)


def train_model(
    df_train: pd.DataFrame, target_column_name: str, ignored_columns: List[str]
) -> Union[RegressorMixin, ClassifierMixin]:
    """Selects and trains an appropriate supervised learning model with the supplied input data."""
    ...


def test_model(
    df_test: pd.DataFrame, model: Union[RegressorMixin, ClassifierMixin]
) -> pd.DataFrame:
    """Tests the model on a held out test set"""
    ...


if __name__ == "__main__":
    df = pd.read_csv("sample_data_format.csv")
    train_test_dict = create_data_splits(df=df, test_size_absolute=5000)
    df_train, df_test = train_test_dict["df_train"], train_test_dict["df_test"]
    model = train_model(
        df_train, target_column_name="R_T1W", ignored_columns=["Date", "Coin"]
    )
    results = test_model(df_test=df_test, model=model)
